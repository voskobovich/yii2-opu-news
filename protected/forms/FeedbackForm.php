<?php

namespace app\forms;

use app\helpers\Param;
use Yii;
use yii\base\Model;


/**
 * Class FeedbackForm
 * @package app\forms
 */
class FeedbackForm extends Model
{
    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;

    /**
     * @var string
     */
    public $second_name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;


    /** @inheritdoc */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email'], 'required'],
            ['email', 'email'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('feedbackForm', 'First Name'),
            'last_name' => Yii::t('feedbackForm', 'Last Name'),
            'second_name' => Yii::t('feedbackForm', 'Second Name'),
            'email' => Yii::t('feedbackForm', 'E-mail'),
            'phone' => Yii::t('feedbackForm', 'Phone'),
        ];
    }

    /**
     * @return bool
     */
    public function send()
    {
        Yii::$app->mailer->compose('feedback/index', ['model' => $this])
            ->setFrom($this->email)
            ->setTo(Param::get('feedback.email'))
            ->setSubject('Обратная связь')
            ->send();

        Yii::$app->session->setFlash('confirmation_sent');
    }
}