<?php

namespace app\db;


/**
 * Class Schema
 * @package app\db
 */
class Schema extends \yii\db\mysql\Schema
{
    const TYPE_CHAR = 'char';

    const CASCADE = 'cascade';
    const RESTRICT = 'restrict';
}