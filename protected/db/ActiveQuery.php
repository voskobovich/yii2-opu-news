<?php

namespace app\db;

use app\base\db\BaseActiveQuery;


/**
 * Class ActiveQuery
 * @package app\db
 */
class ActiveQuery extends BaseActiveQuery
{
    /**
     * Init
     */
    public function init()
    {
        parent::init();

        /** @var \yii\db\ActiveRecord $tModel */
        $tModel = new $this->modelClass;
        $this->from(['t' => $tModel::tableName()]);
    }

    /**
     * Scope for model status
     * @param int $status
     * @return $this
     */
    public function status($status)
    {
        $this->andWhere(['t.status_key' => $status]);

        return $this;
    }
}