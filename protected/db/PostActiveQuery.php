<?php

namespace app\db;

use app\models\Event;


/**
 * Class PostActiveQuery
 * @package app\db
 */
class PostActiveQuery extends ActiveQuery
{
    /**
     * Только видимые
     * @return $this
     */
    public function visible()
    {
        $this->andWhere(['status_key' => Event::STATUS_SHOW]);

        return $this;
    }
}