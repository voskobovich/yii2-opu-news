<?php

namespace app\db;

use app\models\Event;


/**
 * Class EventActiveQuery
 * @package app\db
 */
class EventActiveQuery extends ActiveQuery
{
    /**
     * Только видимые
     * @return $this
     */
    public function visible()
    {
        $this->andWhere(['status_key' => Event::STATUS_SHOW]);

        return $this;
    }
}