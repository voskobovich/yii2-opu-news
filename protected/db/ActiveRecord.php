<?php

namespace app\db;

use app\data\ActiveDataProvider;
use Yii;
use app\helpers\DateHelper;


/**
 * Class ActiveRecord
 * @package app\db
 *
 * @property integer id
 */
abstract class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * Поиск моделей
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->hasAttribute('position')) {
            $dataProvider->sort = [
                'defaultOrder' => [
                    'position' => SORT_ASC,
                ],
            ];
        }

        // Загружаем данные с формы в модель
        if (!$this->load($params)) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Данные для элемента формы DropDownList
     * Возвращает массив записей или пустой массив
     *
     * @param string $indexField - атрибут индексации
     * @param string $labelField - атрибут отображаемого имени
     * @return array
     */
    public static function listAll($indexField = 'id', $labelField = 'name')
    {
        $models = static::find()
            ->asArray()
            ->all();
        $items = array_column($models, $labelField, $indexField);

        return (count($items) > 0) ? $items : [];
    }

    /**
     * Проверка измененности атрибута после сохранения модели
     * @param $attributeName
     * @param $changedAttributes
     * @return bool
     */
    public function isAttributeModified($attributeName, $changedAttributes)
    {
        return isset($changedAttributes[$attributeName]) &&
        $changedAttributes[$attributeName] != $this->oldAttributes[$attributeName];
    }

    /**
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * Поиск модели по Primary Key
     * @param $id
     * @param null $where
     * @return mixed
     */
    public static function findByPk($id, $where = null)
    {
        /** @var ActiveRecord $model */
        $query = static::find()->andWhere('id = :id', [':id' => $id]);

        if (is_array($where)) {
            $query->where($where);
        }

        return $query->one();
    }

    /**
     * Дата создания
     * @param string $format
     * @param bool $plural
     * @return string
     */
    public function getCreated($format = 'd M Y H:i', $plural = true)
    {
        if (empty($this->created_at)) {
            return '-';
        }

        return DateHelper::dateFormatted($this->created_at, $format, $plural);
    }

    /**
     * Дата обновления
     * @param string $format
     * @param bool $plural
     * @return string
     */
    public function getUpdated($format = 'd M Y H:i', $plural = true)
    {
        if (empty($this->updated_at)) {
            return '-';
        }

        return DateHelper::dateFormatted($this->updated_at, $format, $plural);
    }

    /**
     * Возвращает имя поля для ошибки
     * @return mixed
     */
    public function getNameForError()
    {
        return $this->id;
    }
}