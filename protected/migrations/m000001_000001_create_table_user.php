<?php

use app\db\Schema;
use yii\db\Migration;

class m000001_000001_create_table_user extends Migration
{
    private $_tableName = '{{%user}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_tableName, [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'first_name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'last_name' => Schema::TYPE_STRING . '(100) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'confirm_hash' => Schema::TYPE_CHAR . '(32) NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'status_key' => Schema::TYPE_SMALLINT. '(1) NOT NULL DEFAULT 0',
            'role_key' => Schema::TYPE_STRING. '(5) NOT NULL',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'updated_at' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}
