<?php

use app\db\SettingsMigration;
use app\models\Setting;

class m000001_000004_add_ganalytics_settings extends SettingsMigration
{
    protected $_rows = [
        [
            self::FIELD_SECTION => 'ganalytics',
            self::FIELD_KEY => 'code',
            self::FIELD_NAME => 'Google analytics code',
            self::FIELD_VALUE => '',
            self::FIELD_TYPE => Setting::TYPE_TEXTAREA,
            self::FIELD_RULES => [
                ['string'],
            ]
        ],
    ];
}
