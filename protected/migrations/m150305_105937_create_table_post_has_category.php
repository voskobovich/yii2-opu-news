<?php

use app\db\Schema;
use yii\db\Migration;

class m150305_105937_create_table_post_has_category extends Migration
{
    private $_tableName = '{{%post_has_category}}';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->_tableName, [
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (' . DB_QUOTE . 'post_id' . DB_QUOTE . ', ' . DB_QUOTE . 'category_id' . DB_QUOTE . ')'
        ], $tableOptions);

        $this->addForeignKey(
            'fk_post_has_category_category_id1_idx',
            $this->_tableName, 'category_id',
            '{{%category}}', 'id', Schema::RESTRICT,
            Schema::RESTRICT
        );
        $this->addForeignKey(
            'fk_post_has_category_post_idx',
            $this->_tableName, 'post_id',
            '{{%post}}', 'id', Schema::CASCADE,
            Schema::CASCADE
        );
    }

    public function safeDown()
    {
        $this->dropTable($this->_tableName);
    }
}
