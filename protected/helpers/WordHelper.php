<?php

namespace app\helpers;

use yii\base\Object;


/**
 * Class WordHelper
 * @package app\helpers
 */
class WordHelper extends Object
{
    /**
     * Преобразование числа
     * @param $n
     * @param $forms
     * @return mixed
     */
    public static function plural($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }
}