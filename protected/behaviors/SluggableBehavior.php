<?php

namespace app\behaviors;

use Zelenin\yii\behaviors\Slug;


/**
 * Class SluggableBehavior
 * @package app\behaviors
 */
class SluggableBehavior extends Slug
{

}