<?php


namespace app\behaviors;

use app\db\ActiveRecord;
use yii\base\InvalidConfigException;


/**
 * Class SortableGridBehavior
 * @package app\behaviors
 */
class SortableGridBehavior extends \himiklab\sortablegrid\SortableGridBehavior
{
    /**
     * Перед сохранением записи в базу
     * @throws InvalidConfigException
     */
    public function beforeInsert()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        if (!$model->hasAttribute($this->sortableAttribute)) {
            throw new InvalidConfigException("Invalid sortable attribute `{$this->sortableAttribute}`.");
        }

        $maxOrder = $model->find()->max($this->sortableAttribute);
        $model->{$this->sortableAttribute} = $maxOrder + 1;
    }
} 