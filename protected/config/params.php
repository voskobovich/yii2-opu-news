<?php

return [
    // Название приложения
    'app.name' => '<b>Yii2 Basic</b> CMS',

    // Количество строк в таблице админки
    'app.countOnPage' => 25,
];