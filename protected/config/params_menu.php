<?php

return [
    // Backend Menu Items
    'backend.menu' => [
        // Main navigation
        ['label' => Yii::t('general', 'MAIN NAVIGATION'), 'options' => ['class' => 'header']],
        ['label' => '<i class="fa fa-group fa-fw"></i> Записи', 'url' => ['/backend/post/index']],
        ['label' => '<i class="fa fa-group fa-fw"></i> Категории', 'url' => ['/backend/category/index']],
        ['label' => '<i class="fa fa-group fa-fw"></i> События', 'url' => ['/backend/event/index']],
        ['label' => '<i class="fa fa-group fa-fw"></i> Пользователи', 'url' => ['/backend/user/index']],
        // Additional
        ['label' => Yii::t('general', 'ADDITIONAL'), 'options' => ['class' => 'header']],
        [
            'label' => '<i class="fa fa-cogs fa-fw"></i> Настройки',
            'url' => ['/backend/setting/index', 'section' => 'general']
        ],
    ],
    // Settings Menu Items
    'settings.menu' => [
        ['label' => 'General', 'url' => ['/backend/setting/index', 'section' => 'general']],
    ]
];