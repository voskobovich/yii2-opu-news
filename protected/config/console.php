<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$db = require(__DIR__.'/db-local.php');
$i18n = require(__DIR__ . '/i18n.php');

return [
    'id' => 'basic-console',
    'language' => 'en-US',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'controllerNamespace' => 'app\commands',
    'bootstrap' => [
        'log',
        function() {
            define('DB_QUOTE', Yii::$app->db->driverName == 'pgsql' ? '"' : '`');
        }
    ],
    'modules' => [],
    'components' => [
        'user' => [
            'class' => \yii\web\User::className(),
            'identityClass' => 'app\models\User',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => $i18n,
        'db' => $db,
    ],
    'params' => $params,
];
