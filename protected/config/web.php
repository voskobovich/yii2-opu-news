<?php

$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$db = require(__DIR__ . '/db-local.php');

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'log',
        function() {
            define('DB_QUOTE', Yii::$app->db->driverName == 'pgsql' ? '"' : '`');
        },
        function () {
            $menus = require(__DIR__ . '/params_menu.php');
            Yii::$app->params = array_merge(Yii::$app->params, $menus);
        }
    ],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['backend/auth/login'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'errorHandler' => [
            'errorAction' => 'system/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'post/index',
                // Backend Routing
                'backend' => 'backend/user/index',
                'backend/setting/<section:\w+>' => 'backend/setting/index',

                // Default
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:[a-zA-Z-]*>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:[a-zA-Z-]*>' => '<controller>/<action>',
            ]
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/basic/views',
                    '@app/modules' => '@app/themes/basic/views/modules',
                    '@app/widgets' => '@app/themes/basic/widgets',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/translates',
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

return $config;
