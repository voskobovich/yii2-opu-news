<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class BackendAsset
 * @package app\assets
 */
class BackendAsset extends AssetBundle
{
	public $sourcePath = '@app/assets';
    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        // Ionicons
        '//code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css',
        // Theme style
        'css/AdminLTE.css',
        'css/skins/skin-blue.min.css',
        'plugins/datatables/dataTables.bootstrap.css',
    ];
    public $js = [
        'js/shifty.js',
        'js/app.js',
    ];
	public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
	];
}
