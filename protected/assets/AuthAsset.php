<?php

namespace app\assets;

use yii\web\AssetBundle;


/**
 * Class AuthAsset
 * @package app\assets
 */
class AuthAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';
	public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css',
        'css/AdminLTE.css',
	];
	public $js = [
	];
	public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	];
}
