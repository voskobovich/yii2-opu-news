<?php

namespace app\widgets;

use app\models\Event;
use Yii;
use yii\base\Widget;


/**
 * Class EventsList
 * @package app\widgets
 */
class EventsList extends Widget
{
    /**
     * Работаем!
     * @return string|void
     */
    public function run()
    {
        $query = Event::find()
            ->visible()
            ->orderBy(['created_at' => SORT_DESC]);

        return $this->render('eventsList', ['models' => $query->all()]);
    }
}