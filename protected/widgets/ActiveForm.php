<?php

namespace app\widgets;

/**
 * Class ActiveForm
 * @package app\widgets
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
    public $fieldClass = 'app\widgets\ActiveField';
}