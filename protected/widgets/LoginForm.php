<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;


/**
 * Class LoginForm
 * @package app\widgets
 */
class LoginForm extends Widget
{
	/**
	 * Имя модели
	 * @var string
	 */
    private $_model = 'app\forms\LoginForm';

	/**
	 * Работаем!
	 * @return string|void
	 */
	public function run()
	{
        $model = new $this->_model;

        return $this->render('loginForm', ['model' => $model]);
	}
}