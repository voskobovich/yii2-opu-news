<?php

namespace app\commands;

use app\models\User;
use vova07\imperavi\helpers\FileHelper;
use Yii;
use yii\console\Controller;


/**
 * Генератор правил RBAC менеджера
 * Class InstallController
 * @package console\controllers
 */
class InstallController extends Controller
{
    /**
     * Генерация правил RBAC
     */
    public function actionIndex()
    {
        $this->initRBAC();
        $this->createUser();
    }

    /**
     * Инициализация ролей
     */
    private function initRBAC()
    {
        /** @var \yii\rbac\PhpManager $authManager */
        $authManager = Yii::$app->authManager;
        $authManager->removeAll();

        /**
         * Объявляем роли
         */
        $guest = $authManager->createRole('guest');
        $user = $authManager->createRole('user');
        $moder = $authManager->createRole('moder');
        $admin = $authManager->createRole('admin');


        /**
         * Подключаем роли
         */
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($moder);
        $authManager->add($admin);


        /**
         * Устанавливаем наследование ролей
         */
        // Юзер наследует Гостя
        $authManager->addChild($user, $guest);
        // Модератор наследует Юзера
        $authManager->addChild($moder, $user);
        // Админ наследует Модератора
        $authManager->addChild($admin, $moder);

        // Права записи на файл assigments.php
        chmod($authManager->assignmentFile, 0777);
    }

    /**
     * Создание пользователя
     */
    private function createUser()
    {
        $email = "admin@admin.ad";
        $pass = "qwerty";

        $user = new User();
        $user->first_name = 'Admin';
        $user->last_name = 'System';
        $user->email = $email;
        $user->password = $user->confirm_password = $pass;
        $user->role_key = $user::ROLE_ADMIN;
        $user->status_key = $user::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->generateConfirmHash();
        if ($user->save()) {
            echo "{$email} : {$pass}" . PHP_EOL;
        } else {
            print_r($user->getErrors());
            print_r($user->getAttributes());
        }
    }
}