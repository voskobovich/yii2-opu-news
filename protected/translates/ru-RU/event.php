<?php
return [

    // Attributes
    'ID' => 'ID',
    'Name' => 'Название',
    'Description' => 'Описание',
    'Content' => 'Контент',
    'Status' => 'Статус',
    'Created' => 'Создано',
    'Updated' => 'Обновлено',
    'Category List' => 'Список категорий',
    'Temp' => 'Временный',
    'Show' => 'Показан',
    'Hide' => 'Скрыт',

];