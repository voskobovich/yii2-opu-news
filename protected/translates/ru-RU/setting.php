<?php
return [

    // Attributes
    'Section' => 'Секция',
    'Key' => 'Ключ',
    'Name' => 'Название',
    'Hint' => 'Подсказка',
    'Value' => 'Значение',
    'Input type' => 'Тип поля',
    'Variants' => 'Варианты',
    'Rules' => 'Правила',
    'Position' => 'Позиция',
    // Messages
    'Must be array' => 'Должен быть массивом',
    '' => '',

];