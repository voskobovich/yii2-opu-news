<?php
return [

    // Attributes
    'Email' => 'Эл. адрес',
    'Password' => 'Пароль',
    'Remember me' => 'Запомнить меня',
    // Messages
    'Password incorrect' => 'Пароль не верный',
    '' => '',

];