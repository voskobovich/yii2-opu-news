<?php
return [

    // Attributes
    'E-mail' => 'Эл. адрес',
    // Messages
    'There is no user with such email' => 'Учетная запись не найдена',
    'You need to confirm your email address' => 'Вы должны подтвердить адрес электронной почты',

];