<?php
return [

    // Alerts
    'Saved successfully!' => 'Успешно сохранено',
    'Error saving!' => 'Ошибка сохранения',
    'Can\'t delete entity. I\'ts in use' => 'Нельзя удалить, используется',
    // Buttons
    'Save' => 'Сохранить',
    'Create new' => 'Создать еще',
    'Create' => 'Создать',
    // Authorization
    'Email' => 'Эл. адрес',
    'Password' => 'Пароль',
    'Login' => 'Войти',
    // Titles
    'Dashboard | Sign In' => 'Админпанель | Вход',
    'Settings' => 'Настройки',
    'Users' => 'Пользователи',
    // Interfaces
    'Control' => 'Управление',
    'New user' => 'Новый пользователь',
    'Main properties' => 'Основное',
    'Auth information' => 'Данные авторизации',
    'Additional info' => 'Дополнительно',
    'Select action' => 'Выберите действие',
    'Per page: ' => 'На странице: ',
    'Sign in to start your session' => 'Войдите, чтобы начать новую сессию',
];