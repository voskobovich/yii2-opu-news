<?php
return [

    // Attributes
    'ID' => 'ID',
    'First name' => 'Имя',
    'Last name' => 'Фамилия',
    'E-mail' => 'Эл. почта',
    'Status' => 'Статус',
    'Role' => 'Роль',
    'Password' => 'Пароль',
    'Confirm password' => 'Повторите пароль',
    'Confirm hash' => 'Хеш подтверждения',
    'Created' => 'Создан',
    'Updated' => 'Обновлен',
    // Statuses
    'Not active' => 'Не активен',
    'Active' => 'Активен',
    'Banned' => 'Забанен',
    // Roles
    'User' => 'Пользователь',
    'Admin' => 'Администратор',
    'Moderator' => 'Модератор',

];