<?php
return [

    // Alerts
    'Profile is changed' => 'Профиль изменен',
    'Saving error. Please try again' => 'Ошибка сохранения. Попробуйте позже...',
    'User not found' => 'Учетная запись не найдена',
    '' => '',

];