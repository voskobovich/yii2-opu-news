<?php

use app\helpers\AlertHelper;
use app\helpers\Param;
use app\helpers\Html;
use app\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\widgets\Menu;


/**
 * @var \yii\web\View $this
 * @var \app\models\Setting $settingModel
 * @var \app\forms\SettingForm $settingForm
 */

$this->title = Yii::t('backend', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-7">
        <?php AlertHelper::show(); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-7">
        <div class="nav-tabs-custom">
            <?= Menu::widget([
                'options' => ['class' => 'nav nav-tabs'],
                'items' => Param::get('settings.menu'),
            ]) ?>
            <div class="tab-content">
                <div class="tab-pane active">
                    <?php $form = ActiveForm::begin([
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
                            'labelOptions' => ['class' => 'col-sm-2 control-label'],
                        ],
                    ]); ?>
                    <?php foreach ($settingForm->getSettings() as $key => $settingModel) : ?>
                        <?php
                        if ($settingModel->type == $settingModel::TYPE_TEXT) {
                            $field = $form->field($settingForm, $key);
                        }
                        if ($settingModel->type == $settingModel::TYPE_TEXTAREA) {
                            $field = $form->field($settingForm, $key)->textarea(['rows' => 8]);
                        }
                        if ($settingModel->type == $settingModel::TYPE_EDITOR) {
                            $field = $form->field($settingForm, $key)->widget(Widget::className(), [
                                'settings' => [
                                    'lang' => 'ru',
                                    'minHeight' => 200,
                                    'plugins' => [
                                        'fontcolor',
                                        'fontfamily',
                                        'fontsize',
                                        'clips',
                                        'fullscreen',
                                        'counter',
                                        'imagemanager',
                                        'table',
                                        'video',
                                        'textexpander',
                                    ]
                                ]
                            ]);
                        }
                        if ($settingModel->type == $settingModel::TYPE_SELECTBOX) {
                            $field = $form->field($settingForm, $key)->dropDownList($settingModel->getVariants());
                        }
                        if ($settingModel->type == $settingModel::TYPE_SELECTBOX_MULTIPLE) {
                            $field = $form->field($settingForm, $key)->dropDownList($settingModel->getVariants(),
                                ['multiple' => true]);
                        }
                        if ($settingModel->type == $settingModel::TYPE_CHECKBOX) {
                            $field = $form->field($settingForm, $key)->checkbox();
                        }
                        if ($settingModel->type == $settingModel::TYPE_RADIO) {
                            $field = $form->field($settingForm, $key)->radio();
                        }
                        if ($settingModel->type == $settingModel::TYPE_RADIOLIST) {
                            $field = $form->field($settingForm, $key)->radioList($settingModel->getVariants());
                        }

                        echo $field->hint($settingModel->hint);
                        ?>
                    <?php endforeach; ?>

                    <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary']) ?>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>