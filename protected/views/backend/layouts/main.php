<?php

use app\helpers\Html;
use app\helpers\Param;
use app\assets\BackendAsset;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;


/**
 * @var \yii\web\View $this
 * @var string $content
 * @var \app\models\User $userModel
 */

$baseUrl = BackendAsset::register($this)->baseUrl;
$userModel = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>

    <body class="skin-blue">
    <?php $this->beginBody() ?>
        <div class="wrapper">
            <!-- Main Header -->
            <header class="main-header">
                <!-- Logo -->
                <a href="<?=Url::home()?>" class="logo"><?=Param::get('app.name');?></a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="user user-menu">
                                <a href="<?=Url::toRoute(['/backend/auth/logout'])?>">
                                    <i class="glyphicon glyphicons-log-out"></i>
                                    <span><?= Yii::t('backend', 'Logout') ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?=$baseUrl?>/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p><?=$userModel->getFullName() ?></p>
                        <!-- Status -->
                        <a href="<?=Url::toRoute(['/backend/auth/logout'])?>">
                            <i class="fa fa-circle text-success"></i> <?= Yii::t('backend', 'Logout') ?>
                        </a>
                    </div>
                </div>
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- search form (Optional) -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                          <span class="input-group-btn">
                            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <?=Menu::widget([
                        'options' => ['class'=>'sidebar-menu'],
                        'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                        'encodeLabels' => false,
                        'items' => Param::get('backend.menu'),
                    ]);?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?=$this->title?>
                        <small><?= Yii::t('backend', 'Control') ?></small>
                    </h1>
                    <?=Breadcrumbs::widget([
                        'tag' => 'ol',
                        'encodeLabels' => false,
                        'links' => $this->params['breadcrumbs'],
                    ]); ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?= $content ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

        </div><!-- ./wrapper -->
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>