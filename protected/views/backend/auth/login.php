<?php

use app\helpers\Param;
use app\helpers\Html;
use yii\bootstrap\ActiveForm;


/**
 * @var \yii\web\View $this
 * @var \app\forms\LoginForm $model
 */

$this->title = Yii::t('backend', 'Dashboard | Sign In');
?>

<div class="login-box">
    <div class="login-logo">
        <?=Param::get('app.name')?>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?=Yii::t('backend', 'Sign in to start your session')?></p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
        ]); ?>
        <?= $form->field($model, 'email')->input('string',
            ['placeholder' => Yii::t('backend', 'Email')])->label(false) ?>
        <?= $form->field($model, 'password')->passwordInput([
            'placeholder' => Yii::t('backend', 'Password')
        ])->label(false) ?>
            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <?= Html::submitButton(Yii::t('backend', 'Login'),
                        ['class' => 'btn bg-olive btn-block', 'name' => 'login-button']) ?>
                </div><!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->