<?php

use app\widgets\GridView;
use app\helpers\AlertHelper;
use app\widgets\Pjax;
use app\models\User;


/**
 * @var \yii\web\View $this
 * @var \yii\bootstrap\ActiveForm $form
 * @var \app\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <?php AlertHelper::show(); ?>

        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
            'enableShiftyPlugin' => false,
            'actionList' => null,
                'tableOptions' => ['class' => 'table table-striped'],
                'columns' => [
                    'email',
                    [
                        'attribute' => 'role_key',
                        'filter' => User::getRoles(),
                        'value' => function ($model) {
                            return $model->role;
                        }
                    ],
                    [
                        'attribute' => 'status_key',
                        'filter' => User::getStatuses(),
                        'value' => function ($model) {
                            return $model->status;
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'options' => [
                            'width' => '70px'
                        ],
                    ],
                ],
            ]) ?>
        <?php Pjax::end(); ?>
    </div>
</div>