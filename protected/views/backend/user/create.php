<?php

use app\helpers\AlertHelper;


/**
 * @var \yii\web\View $this
 * @var \app\models\User $model
 */

$this->title = Yii::t('backend', 'Users');
$formTitle = Yii::t('backend', 'New user');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $formTitle];
?>

<div class="row">
    <div class="col-lg-12">
        <?php AlertHelper::show(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $this->render('_form', ['model' => $model, 'formTitle' => $formTitle]) ?>
    </div>
</div>