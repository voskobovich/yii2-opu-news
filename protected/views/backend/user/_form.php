<?php

use app\helpers\Html;
use yii\bootstrap\ActiveForm;


/**
 * @var \yii\web\View $this
 * @var \app\models\User $model
 */
?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?=$formTitle?></h3>
    </div><!-- /.box-header -->

    <?php $form = ActiveForm::begin(['id' => 'user-form'], ['role'=>'form']) ?>
        <div class="box-body">

            <legend><?= Yii::t('backend', 'Main properties') ?></legend>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
            <?= $form->field($model, 'role_key')->dropDownList($model->roles) ?>
            <?= $form->field($model, 'status_key')->dropDownList($model->statuses) ?>

            <legend><?= Yii::t('backend', 'Auth information') ?></legend>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'confirm_password')->passwordInput() ?>

            <?php if(!$model->isNewRecord): ?>
                <legend><?= Yii::t('backend', 'Additional info') ?></legend>
                <?= $form->field($model, 'created_at')->textInput(['disabled'=>'disabled']) ?>
                <?= $form->field($model, 'updated_at')->textInput(['disabled'=>'disabled']) ?>
            <?php endif; ?>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <?= Html::submitButton(Yii::t('backend', $model->isNewRecord ? 'Add' : 'Save'),
                ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end() ?>
</div>