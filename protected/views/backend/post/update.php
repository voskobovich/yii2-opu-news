<?php

use app\helpers\AlertHelper;

$this->title = 'Записи';
$formTitle = $model->name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $formTitle];
?>

<div class="row">
    <div class="col-lg-12">
        <?php AlertHelper::show(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $this->render('_form', ['model' => $model, 'formTitle' => $formTitle]) ?>
    </div>
</div>