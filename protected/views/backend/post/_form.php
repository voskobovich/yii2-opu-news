<?php

use app\helpers\Html;
use app\models\Category;
use nex\chosen\Chosen;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

$redactorConfig = [
    'settings' => [
        'lang' => 'en',
        'minHeight' => 200,
        'imageManagerJson' => Url::to(['/backend/image/images-get']),
        'imageUpload' => Url::to(['/backend/image/image-upload']),
        'plugins' => [
            'fontcolor',
            'fontfamily',
            'fontsize',
            'imagemanager',
            'table'
        ]
    ]
];
?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $formTitle ?></h3>
    </div>
    <!-- /.box-header -->

    <?php $form = ActiveForm::begin(['id' => 'post-form', 'options' => ['enctype' => 'multipart/form-data']],
        ['role' => 'form']) ?>
    <div class="box-body">

        <?= $form->field($model, 'name') ?>

        <?= $field = $form->field($model, 'content')->widget(Widget::className(), $redactorConfig); ?>

        <?= $field = $form->field($model, 'description')->widget(Widget::className(), $redactorConfig); ?>

        <?= $form->field($model, 'category_list')->widget(
            Chosen::className(), [
            'items' => Category::listAll(),
            'disableSearch' => 5, // Search input will be disabled while there are fewer than 5 items
            'multiple' => true,
            'clientOptions' => [
                'search_contains' => true,
                'single_backstroke_delete' => false,
            ],
        ]); ?>

        <?= $form->field($model, 'status_key')->dropDownList($model->getStatuses()) ?>

        <?php if (!$model->isNewRecord): ?>
            <?= $form->field($model, 'created_at')->textInput(['disabled' => 'disabled']) ?>
            <?= $form->field($model, 'updated_at')->textInput(['disabled' => 'disabled']) ?>
        <?php endif; ?>

    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend', $model->isNewRecord ? 'Add' : 'Save'),
            ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>