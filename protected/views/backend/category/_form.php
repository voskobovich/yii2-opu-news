<?php
use app\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $formTitle ?></h3>
    </div>
    <!-- /.box-header -->

    <?php $form = ActiveForm::begin(['id' => 'category-form', 'options' => ['enctype' => 'multipart/form-data']],
        ['role' => 'form']) ?>
    <div class="box-body">

        <?= $form->field($model, 'name') ?>

    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <?= Html::submitButton(Yii::t('backend', $model->isNewRecord ? 'Add' : 'Save'),
            ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
