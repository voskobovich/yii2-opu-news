<?php

/**
 * @var yii\web\View $this
 * @var app\models\Post $model
 * @var yii\widgets\ActiveForm $form
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use app\helpers\AlertHelper;
use app\widgets\GridView;
use app\widgets\Pjax;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <?php AlertHelper::show(); ?>

        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'enableShiftyPlugin' => false,
            'actionList' => null,
            'tableOptions' => ['class' => 'table table-striped'],
            'columns' => [
                'name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'options' => [
                        'width' => '70px'
                    ],
                ],
            ],
        ]) ?>
        <?php Pjax::end(); ?>
    </div>
</div>