<?php

namespace app\models;

use app\behaviors\CopyAttributesBehavior;
use app\db\ActiveRecord;
use app\db\PostActiveQuery;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;


/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $content
 * @property integer $status_key
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Category[] $categories
 */
class Post extends ActiveRecord
{
    /**
     * Константы статусов
     */
    const STATUS_TEMP = 0;
    const STATUS_SHOW = 1;
    const STATUS_HIDE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'description', 'category_list'], 'required'],
            [['content'], 'string'],
            [['status_key'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'name' => Yii::t('post', 'Name'),
            'description' => Yii::t('post', 'Description'),
            'content' => Yii::t('post', 'Content'),
            'category_list ' => Yii::t('post', 'Categories'),
            'status_key' => Yii::t('post', 'Status'),
            'created_at' => Yii::t('post', 'Created'),
            'updated_at' => Yii::t('post', 'Updated'),
        ];
    }

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CopyAttributesBehavior::className(),
                'clearTags' => true,
                'maxLength' => 200,
                'attributes' => [
                    'description' => [
                        'attribute' => 'content',
                        'clearTags' => false,
                        'maxLength' => 300,
                    ],
                ],
            ],
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'category_list' => 'categories',
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * @inheritdoc
     * @return PostActiveQuery
     */
    public static function find()
    {
        return new PostActiveQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('{{%post_has_category}}',
            ['post_id' => 'id']);
    }

    /**
     * Список статусов
     * @param null $key
     * @return array
     */
    public static function getStatuses($key = null)
    {
        $items = [
            self::STATUS_TEMP => Yii::t('post', 'Temp'),
            self::STATUS_SHOW => Yii::t('post', 'Show'),
            self::STATUS_HIDE => Yii::t('post', 'Hide'),
        ];

        if (!is_null($key)) {
            return isset($items[$key]) ? $items[$key] : null;
        }

        return $items;
    }

    /**
     * Название статуса
     * @return string
     */
    public function getStatus()
    {
        return self::getStatuses($this->status_key);
    }

    /**
     * Страница просмотра
     * @return string
     */
    public function viewUrl()
    {
        return Url::toRoute(['/post/view', 'id' => $this->id]);
    }
}