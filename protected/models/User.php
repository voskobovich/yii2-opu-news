<?php

namespace app\models;

use app\base\models\BaseUser;


/**
 * User model
 *
 * @property string $first_name
 * @property string $last_name
 */
class User extends BaseUser
{

}