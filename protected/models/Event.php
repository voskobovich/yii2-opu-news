<?php

namespace app\models;

use app\db\ActiveRecord;
use app\db\EventActiveQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;


/**
 * This is the model class for table "{{%event}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $content
 * @property integer $status_key
 * @property string $created_at
 * @property string $updated_at
 */
class Event extends ActiveRecord
{
    /**
     * Константы статусов
     */
    const STATUS_TEMP = 0;
    const STATUS_SHOW = 1;
    const STATUS_HIDE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'description'], 'required'],
            [['content'], 'string'],
            [['status_key'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('event', 'ID'),
            'name' => Yii::t('event', 'Name'),
            'description' => Yii::t('event', 'Description'),
            'content' => Yii::t('event', 'Content'),
            'status_key' => Yii::t('event', 'Status'),
            'created_at' => Yii::t('event', 'Created'),
            'updated_at' => Yii::t('event', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     * @return EventActiveQuery
     */
    public static function find()
    {
        return new EventActiveQuery(get_called_class());
    }

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * Список статусов
     * @param null $key
     * @return array
     */
    public static function getStatuses($key = null)
    {
        $items = [
            self::STATUS_TEMP => Yii::t('post', 'Temp'),
            self::STATUS_SHOW => Yii::t('post', 'Show'),
            self::STATUS_HIDE => Yii::t('post', 'Hide'),
        ];

        if (!is_null($key)) {
            return isset($items[$key]) ? $items[$key] : null;
        }

        return $items;
    }

    /**
     * Название статуса
     * @return string
     */
    public function getStatus()
    {
        return self::getStatuses($this->status_key);
    }

    /**
     * Страница просмотра
     * @return string
     */
    public function viewUrl()
    {
        return Url::toRoute(['/event/view', 'id' => $this->id]);
    }


}