<?php

use app\models\Setting;
use app\themes\basic\assets\ThemeAsset;
use yii\widgets\LinkPager;
use yii\widgets\ListView;


/**
 * @var \yii\web\View $this
 * @var \app\data\ActiveDataProvider $dataProvider
 */

$this->title = Setting::get('general.meta_title');
if ($metaKeywords = Setting::get('general.meta_keywords')) {
    $this->registerMetaTag(['name' => 'keywords', 'content' => $metaKeywords]);
}
if ($metaDescription = Setting::get('general.meta_description')) {
    $this->registerMetaTag(['name' => 'description', 'content' => $metaDescription]);
}

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<div class="col-md-8 content-main">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('/_list_item', ['model' => $model]);
        },
        'layout' => '{items}'
    ]) ?>

    <?php if ($dataProvider->pagination->pageCount > 1): ?>
        <div class="pages">
            <?= LinkPager::widget([
                'pagination' => $dataProvider->getPagination(),
                'options' => [
                    'class' => false
                ]
            ]) ?>
        </div>
    <?php endif; ?>

</div>