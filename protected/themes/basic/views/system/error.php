<?php

use app\helpers\Html;


/**
 * @var \yii\web\View $this
 * @var \yii\web\HttpException $exception
 * @var string $name
 * @var string $message
 */

$this->title = $name;
?>

<?= $exception->statusCode;?>

<?=nl2br(Html::encode($message))?>