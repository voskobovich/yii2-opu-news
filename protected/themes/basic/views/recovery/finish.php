<?php

use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Password recovery';
?>
<?php if (Yii::$app->session->hasFlash('invalid_token')): ?>
    <?php $this->title = 'Recovery token is invalid'; ?>
    <div class="alert alert-danger">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        <p>
            We are sorry but your recovery token is out of date.
            You can try requesting a new one by clicking the link below:
        </p>
        <p>
            <?= Html::a('Request new recovery message', ['/recovery']) ?>
        </p>
    </div>
<?php endif ?>

<?php if (Yii::$app->session->hasFlash('recovery_finished')): ?>
    <?php $this->title = 'Password has been reset'; ?>
    <div class="alert alert-success">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        <p>
            Your password has been successfully changed.
            You can try logging in using your new password
        </p>
    </div>
<?php endif ?>

<?php if (Yii::$app->session->hasFlash('recovery_sent')): ?>
    <?php $this->title = 'Recovery message sent'; ?>
    <div class="alert alert-success">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        <p>
            You have been sent an email with instructions on how to reset your password.
            Please check your email and click the reset link.
        </p>
        <p>
            The email can take a few minutes to arrive. But if you
            are having troubles, you can request a new one.
        </p>
    </div>
<?php endif ?>