<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Password recovery';
?>

<?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'confirm_password')->passwordInput() ?>

<?= Html::submitButton('Confirm') ?>
<?php ActiveForm::end() ?>