<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Password recovery request';
?>
<?php $form = ActiveForm::begin(); ?>
<h1>Password recovery</h1>
    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

<?= Html::submitButton('Send') ?>
<?php ActiveForm::end(); ?>
