<?php

use app\helpers\Html;
use app\models\Setting;
use app\themes\basic\assets\ThemeAsset;
use app\widgets\EventsList;
use yii\helpers\Url;


/**
 * @var \yii\web\View $this
 * @var string $content
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<?php $this->beginPage() ?>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <title><?= Html::encode($this->title) ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="application/x-javascript"> addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);
            function hideURLbar() {
                window.scrollTo(0, 1);
            } </script>

        <link href='http://fonts.googleapis.com/css?family=Oswald:100,400,300,700' rel='stylesheet' type='text/css'>
        <link
            href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,300italic,400italic,600italic'
            rel='stylesheet' type='text/css'>

        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="banner">
        <div class="header">
            <div class="container">
                <div class="logo">
                    <a href="<?= Url::home() ?>"> <img src="<?= $baseUrl ?>/images/logo.png" title="soup"/></a>
                </div>

                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul>
                        <li class="active"><a href="#">HOME</a></li>
                        <li><a href="#">CONTACT</a></li>
                        <li><a href="#">TERMS</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="container">
            <div class="banner-head">
                <h1>Lorem ipsum dolor sit amet</h1>

                <h2>cliquam tincidunt mauris</h2>
            </div>
            <div class="banner-links">
                <ul>
                    <li class="active"><a href="#">LOREM IPSUM</a></li>
                    <li><a href="#">DOLAR SITE AMET</a></li>
                    <li><a href="#">MORBI IN SEM</a></li>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container">

            <div class="content-grids">

                <?= $content ?>

                <div class="col-md-4 content-main-right">
                    <div class="search">
                        <h3>ПОИСК МАТЕРИАЛОВ</h3>
                        <?= Html::beginForm(['/search/index'], 'get'); ?>
                        <?= Html::textInput('q') ?>
                            <input type="submit" value="">
                        <?= Html::endForm(); ?>
                    </div>
                    <?= EventsList::widget() ?>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>

    <div class="copywrite">
        <div class="container">
            <p>Copyrights &copy; 2015 Blogging All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a>
            </p>
        </div>
    </div>

    <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

    <?= Setting::get('ganalytics.code') ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>