<?php

use app\helpers\Html;
use app\themes\basic\assets\ThemeAsset;
use yii\widgets\ActiveForm;


/**
 * @var \yii\web\View $this
 * @var \app\forms\FeedbackForm $model
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<div class="col-md-8 content-main">
    <div class="content-grid">
        <div class="content-grid-single">
            <div class="content-form">
                <h3>Обратная связь</h3>

                <?php if (!Yii::$app->session->hasFlash('success')): ?>

                    <?php $form = ActiveForm::begin([
                        'id' => 'feedback-form',
                    ]) ?>
                    <?= $form->field($model, 'last_name') ?>
                    <?= $form->field($model, 'first_name') ?>
                    <?= $form->field($model, 'second_name') ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'phone') ?>

                    <?= Html::submitButton('SEND') ?>
                    <?php ActiveForm::end() ?>

                <?php else: ?>
                    Сообщение успешно отправлено!
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>