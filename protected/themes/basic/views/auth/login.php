<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Sign In';
?>
<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'enableAjaxValidation' => true,
]); ?>
<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email']) ?>
<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password']) ?>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>

<?= Html::submitButton('Login') ?>

<?= Html::a('Password recovery', ['/recovery']) ?>
<?php ActiveForm::end(); ?>