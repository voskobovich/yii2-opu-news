<?php

use app\themes\basic\assets\ThemeAsset;


/**
 * @var \yii\web\View $this
 * @var \app\models\Post $model
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<div class="content-grid-sec">
    <div class="content-sec-info">
        <h3><a href="<?= $model->viewUrl() ?>"><?= $model->name ?></a></h3>
        <?= $model->description ?>
        <a class="bttn" href="<?= $model->viewUrl() ?>">Читать далее</a>
    </div>
</div>