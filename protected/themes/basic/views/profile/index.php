<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Profile edit';
?>
<div>
    <h1><?=$this->title?></h1>
    <div class="profileBlock">
        <?php if (Yii::$app->session->hasFlash('editMessage')): ?>
            <div class="alert alert-danger">
                <h4>
                    <?= Yii::$app->session->getFlash('editMessage') ?>
                </h4>
            </div>
        <?php endif ?>

        <?php $form = ActiveForm::begin([]) ?>
            <?= $form->field($model, 'email')->textInput(['disabled'=>true]) ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'confirm_password')->passwordInput() ?>
            <?= $form->field($model, 'delivery_zip') ?>
            <?= $form->field($model, 'delivery_country') ?>
            <?= $form->field($model, 'delivery_city') ?>
            <?= $form->field($model, 'delivery_state') ?>
            <?= $form->field($model, 'delivery_address') ?>
            <?= $form->field($model, 'delivery_phone') ?>

        <?= Html::submitButton('Save', ['class' => 'btn']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>