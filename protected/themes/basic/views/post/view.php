<?php

use app\themes\basic\assets\ThemeAsset;


/**
 * @var \yii\web\View $this
 * @var \app\models\Post $model
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<div class="col-md-8 content-main">
    <div class="content-grid">
        <div class="content-grid-head">
            <h4><?= $model->created_at ?></h4>

            <div class="clearfix"></div>
        </div>
        <div class="content-grid-single">
            <h3><?= $model->name ?></h3>
            <?= $model->content ?>
        </div>

    </div>
</div>