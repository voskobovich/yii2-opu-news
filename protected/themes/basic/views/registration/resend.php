<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Request new confirmation message';
?>
<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

<?= Html::submitButton('Send') ?>
<?php ActiveForm::end() ?>