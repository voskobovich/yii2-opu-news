<?php

use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Registration';
?>
<h1><?=$this->title?></h1>

<?php if (Yii::$app->session->hasFlash('confirmation_sent')): ?>
    <?php $this->title = 'We need to confirm your email address'; ?>
    <div class="alert alert-info">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        <p>
            Please check your email and click the confirmation link to complete your registration.
            The email can take a few minutes to arrive.
            But if you are having troubles, you can request a new one by clicking the link below:
        </p>
        <p>
            <?= Html::a('Request new confirmation message', ['/registration/resend']) ?>
        </p>
    </div>
<?php endif ?>

<?php if (Yii::$app->session->hasFlash('invalid_token')): ?>
    <?php $this->title = 'Invalid token'; ?>
    <div class="alert alert-danger">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        <p>
            We are sorry but your confirmation token is out of date.
            You can try requesting a new one by clicking the link below:
        </p>
        <p>
            <?= Html::a('Request new confirmation message', ['/registration/resend']) ?>
        </p>
    </div>
<?php endif ?>

<?php if (Yii::$app->session->hasFlash('confirmation_finished')): ?>
    <?php $this->title = 'Your account has been confirmed'; ?>
    <div class="alert alert-success">
        <h4>
            <?= Html::encode($this->title) ?>
        </h4>
        Awesome! You have successfully confirmed your
        email address. You may sign in using your credentials now.
    </div>
<?php endif ?>