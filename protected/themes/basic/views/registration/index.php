<?php

use app\widgets\ActiveForm;
use app\helpers\Html;


/**
 * @var \yii\web\View $this
 */

$this->title = 'Registration';
?>
<h1><?=$this->title?></h1>

<?php $form = ActiveForm::begin(['id' => 'register-form']) ?>
    <?= $form->field($model, 'first_name') ?>
    <?= $form->field($model, 'last_name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'confirm_password')->passwordInput() ?>

<?= Html::submitButton('Register') ?>
<?php ActiveForm::end() ?>


