<?php

use app\themes\basic\assets\ThemeAsset;


/**
 * @var \yii\web\View $this
 * @var \app\models\Event[] $models
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>

<?php if (!empty($models)): ?>
    <div class="categories">
        <h3>События</h3>
        <?php foreach ($models as $model): ?>
            <li><a href="<?= $model->viewUrl() ?>"><?= $model->name ?></a></li>
        <?php endforeach; ?>
    </div>
<?php endif; ?>