<?php

use app\themes\basic\assets\ThemeAsset;
use app\helpers\Html;
use app\widgets\ActiveForm;


/**
 * @var \yii\web\View $this
 */

$baseUrl = ThemeAsset::register($this)->baseUrl;
?>
<?php $form = ActiveForm::begin([
    'action' => ['auth/login'],
    'id' => 'login-form',
    'enableAjaxValidation' => true,
]); ?>
<div>
    <div>
            <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email']) ?>
        </div>
    <div>
            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password']) ?>
        </div>
    <div>
        <?= Html::activeCheckbox($model, 'rememberMe'); ?>
        </div>
    </div>

<div>
    <?= Html::submitButton('Login') ?><br>

    <div>
            <?= Html::a('Password recovery', ['/recovery']) ?>
            or <?= Html::a('Register', ['/registration']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
