<?php

namespace app\themes\basic\assets;

use yii\web\AssetBundle;


/**
 * Class ThemeAsset
 * @package app\themes\basic\assets
 */
class ThemeAsset extends AssetBundle
{
    // Путь к папке ресурсов
    public $sourcePath = '@app/themes/basic/assets';

    // Стили
    public $css = [
        'css/style.css'
    ];

    // Скрипты
    public $js = [
        'js/easing.js',
        'js/move-top.js',
        'js/main.js',
    ];

    // Зависимости
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}
