<?php

namespace app\base\controllers;

use app\extensions\Frontend;
use app\models\User;
use Yii;


/**
 * Class BaseProfileController
 * @package app\base\controllers
 */
abstract class BaseProfileController extends Frontend
{
    /** @var User $userModel */
    public $userModel = null;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!Yii::$app->user->isGuest) {
            $this->userModel = User::findByPk(Yii::$app->user->id);
        }

        if (!$this->userModel) {
            $this->redirect(['/auth/login']);
        }

        return parent::beforeAction($action);
    }

    /**
     * Изменение профиля пользователя
     * @return string
     */
    public function actionIndex()
    {
        $post = Yii::$app->getRequest()->post();

        if ($post) {
            $this->userModel->load($post);

            if ($this->userModel->save()) {
                Yii::$app->session->setFlash('editMessage', Yii::t('frontend', 'Profile is changed'));
            } else {
                Yii::$app->session->setFlash('editMessage', Yii::t('frontend', 'Saving error. Please try again'));
            }
        }

        return $this->render('index', [
            'model' => $this->userModel
        ]);
    }

    /**
     * Список заказов пользователя
     * @return string
     */
    public function actionOrders()
    {
        return $this->render('orders', [
            'model' => $this->userModel
        ]);
    }
} 