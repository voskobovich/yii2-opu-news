<?php

namespace app\base\controllers;

use app\extensions\Frontend;


/**
 * Class BaseSystemController
 * @package app\base\controllers
 */
abstract class BaseSystemController extends Frontend
{
    /**
     * Внешние экшены
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
