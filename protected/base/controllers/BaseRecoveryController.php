<?php

namespace app\base\controllers;

use app\extensions\Frontend;
use app\forms\RecoveryForm;
use app\forms\RecoveryRequestForm;
use app\models\User;
use yii\filters\AccessControl;
use Yii;


/**
 * Class BaseRecoveryController
 * @package app\base\controllers
 */
abstract class BaseRecoveryController extends Frontend
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'reset'],
                        'roles' => ['?']
                    ],
                ]
            ],
        ];
    }

    /**
     * Запрос на восстановление пароля пользователя
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = new RecoveryRequestForm();
        if ($model->load(\Yii::$app->getRequest()->post()) && $model->sendRecoveryMessage()) {
            return $this->render('finish');
        }
        return $this->render('request', [
            'model' => $model
        ]);
    }

    /**
     * Изменение пароля пользователя
     *
     * @param  integer $id
     * @param  string $code
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset($id, $code)
    {
        /** @var \app\models\User $userModel */
        $userModel = User::findByPk($id);

        if (!$userModel->validateConfirmHash($code)) {
            Yii::$app->session->setFlash('invalid_token');
            $this->redirect(['finish']);
        }

        $recoveryForm = new RecoveryForm();
        $recoveryForm->email = $userModel->email;

        if ($recoveryForm->load(Yii::$app->getRequest()->post()) && $recoveryForm->resetPassword()) {
            return $this->render('finish');
        }

        return $this->render('reset', [
            'model' => $recoveryForm
        ]);
    }
} 