<?php

namespace app\base\controllers;

use app\helpers\HttpError;
use Yii;
use app\forms\ResendForm;
use yii\filters\AccessControl;
use app\extensions\Frontend;
use app\models\User;
use yii\web\NotFoundHttpException;


/**
 * Class BaseRegistrationController
 * @package app\base\controllers
 */
abstract class BaseRegistrationController extends Frontend
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'confirm', 'resend'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['finish'],
                        'roles' => ['?', '@']
                    ],
                ]
            ],
        ];
    }

    /**
     * Регистрация пользователя
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $userModel = new User();

        $post = Yii::$app->request->post();
        $userIsLoaded = $userModel->load($post);

        if ($userIsLoaded && $userModel->register()) {
            Yii::$app->session->setFlash('confirmation_sent');
            $this->redirect(['finish']);
        }

        return $this->render('index', [
            'model' => $userModel,
        ]);
    }

    /**
     * Страница информации
     * @return string
     */
    public function actionFinish()
    {
        return $this->render('finish');
    }

    /**
     * Подтверждение регистрации пользователем
     * @param $id
     * @param $code
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConfirm($id, $code)
    {
        /** @var \app\models\User $user */
        $user = User::findByPk($id);

        if (!empty($user)) {
            if ($user->validateConfirmHash($code)) {
                $user->status_key = User::STATUS_ACTIVE;
                $user->confirm_hash = null;

                if ($user->save()) {
                    Yii::$app->user->login($user);
                    Yii::$app->session->setFlash('confirmation_finished');
                }
            } else {
                Yii::$app->session->setFlash('invalid_token');
            }
        } else {
            HttpError::the404(Yii::t('frontend', 'User not found'));
        }

        return $this->redirect(['finish']);
    }

    /**
     * Переотправка кода подтверждения
     * @return string
     */
    public function actionResend()
    {
        $model = new ResendForm();
        $post = Yii::$app->request->post();

        if (!empty($post)) {
            if ($model->load($post) && $model->resend()) {
                Yii::$app->session->setFlash('confirmation_sent');
                return $this->redirect(['finish']);
            } else {
                HttpError::the400();
            }
        }

        return $this->render('resend', [
            'model' => $model
        ]);
    }
}