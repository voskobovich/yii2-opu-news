<?php

namespace app\base\forms;

use app\models\User;
use Yii;
use yii\base\Model;


/**
 * Class BaseRecoveryForm
 * @package app\base\forms
 */
abstract class BaseRecoveryForm extends Model
{
    /**
     * @var string
     */
    public $password;
    public $confirm_password;

    public $email;

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('recoveryForm', 'E-mail'),
            'password' => Yii::t('recoveryForm', 'Password'),
            'confirm_password' => Yii::t('recoveryForm', 'Confirm password'),
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 4],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Resets user's password.
     *
     * @return bool
     */
    public function resetPassword()
    {
        if ($this->validate()) {
            $userModel = User::findByEmail($this->email);
            $userModel->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $userModel->password = $this->password;
            $userModel->confirm_password = $this->confirm_password;
            $userModel->confirm_hash = '';

            if ($userModel->save()) {
                Yii::$app->session->setFlash('recovery_finished');

                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'recovery-form';
    }
} 