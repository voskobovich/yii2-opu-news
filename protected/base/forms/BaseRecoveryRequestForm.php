<?php

namespace app\base\forms;

use app\models\Setting;
use app\models\User;
use yii\base\Model;
use yii\helpers\Url;
use Yii;


/**
 * Class BaseRecoveryRequestForm
 * @package app\base\forms
 */
abstract class BaseRecoveryRequestForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * Модель пользователя
     * @var $_user User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('recoveryRequestForm', 'E-mail'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => User::className(),
                'message' => Yii::t('recoveryRequestForm', 'There is no user with such email'),
            ],
            [
                'email',
                function ($attribute) {
                    $this->_user = User::findByEmail($this->email);
                    if ($this->_user !== null && $this->_user->status_key == User::STATUS_NOT_ACTIVE) {
                        $this->addError($attribute,
                            Yii::t('recoveryRequestForm', 'You need to confirm your email address'));
                    }
                }
            ],
        ];
    }

    /**
     * Sends recovery message.
     *
     * @return bool
     */
    public function sendRecoveryMessage()
    {
        if ($this->validate()) {
            /** @var $this ->_user User */
            $this->_user->generateConfirmHash();
            $this->_user->save(false);

            $link = Url::toRoute([
                '/recovery/reset',
                'id' => $this->_user->id,
                'code' => $this->_user->confirm_hash
            ], true);

            Yii::$app->mailer->compose('recovery/index', ['link' => $link])
                ->setFrom(Setting::get('mail.from'))
                ->setTo($this->_user->email)
                ->setSubject(Setting::get('mail.password_reset_subject'))
                ->send();

            \Yii::$app->session->setFlash('recovery_sent');

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'recovery-request-form';
    }
} 