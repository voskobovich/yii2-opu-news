<?php

namespace app\base\forms;

use app\models\Setting;
use app\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\Url;


/**
 * Class BaseResendForm
 * @package app\base\forms
 */
abstract class BaseResendForm extends Model
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var User
     */
    private $_user;

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->_user == null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                function () {
                    if ($this->user != null) {
                        $this->addError('email', Yii::t('resendForm', 'This account has already been confirmed'));
                    }
                }
            ],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('resendForm', 'E-mail'),
        ];
    }

    /** @inheritdoc */
    public function formName()
    {
        return 'resend-form';
    }

    /**
     * Creates new confirmation token and sends it to the user.
     *
     * @return bool
     */
    public function resend()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->generateConfirmHash();
            $user->save(false);

            $link = Url::toRoute([
                '/registration/confirm',
                'id' => $user->id,
                'code' => $user->confirm_hash
            ], true);

            Yii::$app->mailer->compose('registration/index', ['link' => $link])
                ->setFrom(Setting::get('mail.from'))
                ->setTo($user->email)
                ->setSubject('Confirm your account')
                ->send();

            Yii::$app->session->setFlash('confirmation_sent');

            return true;
        }

        return false;
    }
} 