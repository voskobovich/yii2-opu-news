<?php

namespace app\base\models;

use app\data\ActiveDataProvider;
use app\db\ActiveRecord;
use app\db\ActiveQuery;
use app\models\Setting;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer $id
 * @property string $role_key
 * @property string $first_name
 * @property string $last_name
 * @property string $password_hash
 * @property string $password
 * @property string $confirm_password
 * @property string $email
 * @property string $auth_key
 * @property string $confirm_hash
 * @property integer $status_key
 * @property integer $created_at
 * @property integer $updated_at
 */
abstract class BaseUser extends ActiveRecord implements IdentityInterface
{
    /**
     * Статусы активации
     */
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;

    /**
     * Роли пользователя
     */
    const ROLE_ADMIN = 'admin';
    const ROLE_MODER = 'moder';
    const ROLE_USER = 'user';

    /**
     * Поля паролей
     * @var string
     */
    public $password;
    public $confirm_password;

    protected $_realPassword = null;

    /**
     * Название таблицы БД
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * Правила валидации
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['email', 'first_name', 'last_name'], 'required'],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            [['password', 'email'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            ['email', 'unique'],
            ['email', 'email'],
            ['status_key', 'default', 'value' => self::STATUS_NOT_ACTIVE],
            ['status_key', 'in', 'range' => array_keys(self::getStatuses())],
            ['role_key', 'default', 'value' => self::ROLE_USER],
            ['role_key', 'in', 'range' => array_keys(self::getRoles())]
        ];

        /**
         * Пароль обязателен только при создании учетной записи
         */
        if ($this->isNewRecord) {
            $rules[] = [['password', 'confirm_password'], 'required'];
        }

        return $rules;
    }

    /**
     * Имена атрибутов
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'first_name' => Yii::t('user', 'First name'),
            'last_name' => Yii::t('user', 'Last name'),
            'email' => Yii::t('user', 'E-mail'),
            'status_key' => Yii::t('user', 'Status'),
            'role_key' => Yii::t('user', 'Role'),
            'password' => Yii::t('user', 'Password'),
            'confirm_password' => Yii::t('user', 'Confirm password'),
            'confirm_hash' => Yii::t('user', 'Confirm hash'),
            'created_at' => Yii::t('user', 'Created'),
            'updated_at' => Yii::t('user', 'Updated'),
        ];
    }

    /**
     * Поведения
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                },
            ]
        ];
    }

    /**
     * Добавление ограничения на
     * поиск продуктов по статусу
     * @inheritdoc
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * Поиск моделей
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Загружаем данные с формы в модель
        if (!$this->load($params)) {
            return $dataProvider;
        }

        // Фильтры поиска
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['status_key' => $this->status_key]);
        $query->andFilterWhere(['role_key' => $this->role_key]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'created_at', $this->created_at]);
        $query->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }

    /**
     * Перед валидацией
     * @return mixed
     */
    public function beforeValidate()
    {
        if (empty($this->password) && empty($this->confirm_password) && $this->isNewRecord) {
            $this->password = $this->confirm_password = rand(100000, 999999);
        }

        $this->_realPassword = $this->password;

        return parent::beforeValidate();
    }

    /**
     * Перед сохранением
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->generateAuthKey();
        $this->generateConfirmHash();
        $this->updatePassword();

        return parent::beforeSave($insert);
    }

    /**
     * Обновление пароля
     */
    protected function updatePassword()
    {
        if ($this->isNewRecord || (!empty($this->password) && !empty($this->confirm_password) && !$this->isNewRecord)) {
            $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        } else {
            unset($this->password_hash);
        }
    }

    /**
     * После сохранения
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Сбиваем значения с атрибутов пароля
        $this->setAttributes(['password' => null, 'confirm_password' => null]);

        // Переназначаем роль
        Yii::$app->authManager->revokeAll($this->getId());
        $role = Yii::$app->authManager->getRole($this->role_key);
        Yii::$app->authManager->assign($role, $this->getId());

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * После удаления
     * @return bool
     */
    public function afterDelete()
    {
        // Переназначаем роль
        Yii::$app->authManager->revokeAll($this->getId());

        parent::afterDelete();
    }

    /**
     * Поиск по идентификатору
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Поиск пользователя по E-mail
     * @param $email
     * @return static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status_key' => self::STATUS_ACTIVE]);
    }

    /**
     * Геттер идентификатора
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Геттер ключа авторизации
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Список статусов
     * @param null $key
     * @return array
     */
    public static function getStatuses($key = null)
    {
        $items = [
            self::STATUS_NOT_ACTIVE => Yii::t('user', 'Not active'),
            self::STATUS_ACTIVE => Yii::t('user', 'Active'),
            self::STATUS_BANNED => Yii::t('user', 'Banned'),
        ];

        if (!is_null($key)) {
            return isset($items[$key]) ? $items[$key] : null;
        }

        return $items;
    }

    /**
     * Название статуса
     * @return string
     */
    public function getStatus()
    {
        return self::getStatuses($this->status_key);
    }

    /**
     * Список ролей
     * @param null $key
     * @return array
     */
    public static function getRoles($key = null)
    {
        $items = [
            self::ROLE_USER => Yii::t('user', 'User'),
            self::ROLE_ADMIN => Yii::t('user', 'Admin'),
            self::ROLE_MODER => Yii::t('user', 'Moderator')
        ];

        if (!is_null($key)) {
            return isset($items[$key]) ? $items[$key] : null;
        }

        return $items;
    }

    /**
     * Название роли
     * @return string
     */
    public function getRole()
    {
        return self::getRoles($this->role_key);
    }

    /**
     * Генерация ключа авторизации для функции "Запомнить меня"
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * Генерация ключа подтверждения регистрации
     */
    public function generateConfirmHash()
    {
        $this->confirm_hash = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * Поиск по токену доступа
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Проверка ключа авторизации
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Проверка ключа подтверждения аккаунта
     * @inheritdoc
     */
    public function validateConfirmHash($confirmHash)
    {
        return $this->confirm_hash === $confirmHash;
    }

    /**
     * Валидация пароля
     *
     * @param  string $password пароль с формы
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Регистрация пользователя
     * @param bool $send_email
     * @return $this|bool
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function register($send_email = true)
    {
        $transaction = $this->getDb()->beginTransaction();

        try {
            if ($this->save()) {
                $transaction->commit();
                if($send_email) {
                    /**
                     * Отправляем письмо
                     */
                    $link = Url::toRoute([
                        '/registration/confirm',
                        'id' => $this->id,
                        'code' => $this->confirm_hash
                    ], true);

                    Yii::$app->mailer->compose('registration/index', ['link' => $link])
                        ->setFrom(Setting::get('mail.from'))
                        ->setTo($this->email)
                        ->setSubject(Setting::get('mail.confirm_subject'))
                        ->send();
                }

                return $this;
            }
        } catch (HttpException $e) {
            $transaction->rollBack();
        }

        return false;
    }

    /**
     * Получение полного имени пользователя
     * @return array|mixed|string
     */
    public function getFullName()
    {
        return !empty($this->first_name) && !empty($this->last_name) ?
            "{$this->first_name} {$this->last_name}" :
            $this->email;
    }
}