<?php

namespace app\base\db;

use yii\db\ActiveQuery;


/**
 * Class BaseActiveQuery
 * @package app\base\db
 */
abstract class BaseActiveQuery extends ActiveQuery
{
    /**
     * Init
     */
    public function init()
    {
        parent::init();

        /** @var \yii\db\ActiveRecord $tModel */
        $tModel = new $this->modelClass;
        $this->from(['t' => $tModel::tableName()]);
    }

    /**
     * Scope for model status
     * @param int $status
     * @return $this
     */
    public function status($status)
    {
        $this->andWhere(['t.status_key' => $status]);
        return $this;
    }
}