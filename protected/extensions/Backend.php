<?php

namespace app\extensions;

use Exception;
use Yii;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\HttpError;
use app\helpers\AlertHelper;


/**
 * Class Backend
 * @package app\extensions
 */
class Backend extends Controller
{
    /**
     * Путь к макету
     * @var string
     */
    public $layout = '@app/views/backend/layouts/main';

    /**
     * Модель с которой работает контроллер
     * @var string
     */
    public $modelClass = null;

    /**
     * Ссылки редиректа после действий
     */
    public $urlAfterCreate = 'index';
    public $urlAfterUpdate = 'index';
    public $urlAfterDelete = 'index';

    /**
     * Подключение общих поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Initialization backend controller
     * @throws \yii\web\HttpException
     */
    public function init()
    {
        if ($this->modelClass === null) {
            HttpError::the500('Please, set "modelClass" property in your child controller');
        }

        parent::init();
    }

    /**
     * Вывод списка моделей
     * @return string
     */
    public function actionIndex()
    {
        /** @var \app\db\ActiveRecord $model */
        $model = new $this->modelClass;
        $get = Yii::$app->request->get();
        $post = Yii::$app->request->post();
        $dataProvider = $model->search($get);

        if(!empty($post['with']) && !empty($post['action'])) {
            if($post['with'] == 'selected') {
                switch($post['action']) {
                    case 'delete' :
                        $this->deleteSelected($post['selection']);
                        break;
                }
            }
            if($post['with'] == 'all') {
                switch($post['action']) {
                    case 'delete' :
                        $this->deleteAll();
                        break;
                }
            }
        }

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Создание модели
     * @return string
     */
    public function actionCreate()
    {
        /** @var \app\db\ActiveRecord $model */
        $model = new $this->modelClass;
        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success(Yii::t('backend', 'Saved successfully!'));
                $this->redirect([$this->urlAfterCreate]);
            } else {
                AlertHelper::error(Yii::t('backend', 'Error saving!'));
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Обновление модели
     * @param $id
     * @return string
     */
    public function actionUpdate($id)
    {
        /** @var \app\db\ActiveRecord $model */
        $model = new $this->modelClass;
        $model = $model::findByPk($id);

        if (empty($model)) {
            HttpError::the404();
        }

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            if ($model->save()) {
                AlertHelper::success(Yii::t('backend', 'Saved successfully!'));
                $this->redirect([$this->urlAfterUpdate]);
            } else {
                AlertHelper::error(Yii::t('backend', 'Error saving!'));
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Удаление модели
     * @param $id
     * @throws ErrorException
     */
    public function actionDelete($id)
    {
        /** @var \app\db\ActiveRecord $model */
        $model = new $this->modelClass;

        $pkName = $model->primaryKey();
        if(is_array($pkName)) {
            if(count($pkName) > 1) {
                throw new ErrorException('Composite foreign keys are not allowed.');
            }
            $pkName = $pkName[0];
        }
        $pkName = (string)$pkName;

        try {
            if ($model->deleteAll([$pkName => $id])) {
                AlertHelper::success(Yii::t('backend', 'Successfully removed!'));
            } else {
                AlertHelper::error(Yii::t('backend', 'Error removing!'));
            }
        } catch (Exception $ex) {
            AlertHelper::error(Yii::t('backend', 'Can\'t delete entity. I\'ts in use'));
        }

        if (!Yii::$app->request->isAjax) {
            $this->redirect([$this->urlAfterDelete]);
        }
    }

    /**
     * Удаление отмеченных записей
     * @param $selection
     */
    protected function deleteSelected($selection)
    {
        // Если есть выбранные элементы
        if (!empty($selection) && is_array($selection)) {
            // Массив айдишников
            $errors = [];

            // Удаляем каждую запись в базе из нужных
            foreach ($selection as $id) {
                /** @var \app\db\ActiveRecord $model */
                $model = new $this->modelClass;

                $model = $model->findByPk($id);

                if (!empty($model)) {
                    try {
                        $model->delete();
                    } catch (Exception $ex) {
                        $errors[] = $model->id;
                    }
                }
            }

            if (!empty($errors)) {
                AlertHelper::error(Yii::t('app',
                    'Can not delete ids: ' . implode(', ', $errors) . ', they are in use.'));
            } else {
                AlertHelper::success(Yii::t('app', 'Successfully removed!'));
            }
        }
    }

    /**
     * Удаление всех записей
     */
    protected function deleteAll()
    {
        /** @var \app\db\ActiveRecord $model */
        $model = new $this->modelClass;

        $model->deleteAll();
    }

}