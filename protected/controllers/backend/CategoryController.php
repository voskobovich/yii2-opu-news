<?php

namespace app\controllers\backend;

use app\extensions\Backend;

class CategoryController extends Backend
{
    /**
     * Класс модели
     * @var string
     */
    public $modelClass = 'app\models\Category';
}
