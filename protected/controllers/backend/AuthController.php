<?php

namespace app\controllers\backend;

use app\extensions\Frontend;
use Yii;
use yii\web\Response;
use app\widgets\ActiveForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\forms\LoginForm;


/**
 * Class AuthController
 * @package app\controllers\backend
 */
class AuthController extends Frontend
{
    /**
     * Путь к макету
     * @var string
     */
    public $layout = '@app/views/backend/layouts/outer';

    /**
     * Подключение поведений
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    /**
     * Обработка авторизации
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $postData = Yii::$app->request->post();

        if (Yii::$app->request->isAjax) {
            $model->load($postData);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load($postData) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model
            ]);
        }
    }

    /**
     * Обработка разлогинивания
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
