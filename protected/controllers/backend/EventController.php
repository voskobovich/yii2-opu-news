<?php

namespace app\controllers\backend;

use app\extensions\Backend;

class EventController extends Backend
{
    /**
     * Класс модели
     * @var string
     */
    public $modelClass = 'app\models\Event';
}
