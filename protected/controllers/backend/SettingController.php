<?php

namespace app\controllers\backend;

use app\extensions\Backend;
use app\helpers\AlertHelper;
use app\helpers\HttpError;
use app\forms\SettingForm;
use Yii;


/**
 * Class SettingController
 * @package app\controllers\backend
 */
class SettingController extends Backend
{
    /**
     * Отказываемся от стандартного
     * функционала CRUD
     * @var string
     */
    public $modelClass = false;

    /**
     * @param string $section
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex($section = 'general')
    {
        $settingForm = new SettingForm();
        if (!$settingForm->loadBySection($section)) {
            HttpError::the404();
        }

        $post = Yii::$app->request->post();

        if ($post) {
            $settingForm->load($post, 'SettingForm');

            if ($settingForm->save()) {
                AlertHelper::success(Yii::t('backend', 'Saved successfully!'));
            } else {
                AlertHelper::success(Yii::t('backend', 'Error saving!'));
            }
        }

        return $this->render('index', [
            'settingForm' => $settingForm
        ]);
    }
}
