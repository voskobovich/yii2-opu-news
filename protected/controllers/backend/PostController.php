<?php

namespace app\controllers\backend;

use app\extensions\Backend;

class PostController extends Backend
{
    /**
     * Класс модели
     * @var string
     */
    public $modelClass = 'app\models\Post';
}
