<?php

namespace app\controllers\backend;

use app\extensions\Backend;


/**
 * Class UserController
 * @package app\controllers\backend
 */
class UserController extends Backend
{
    /**
     * Класс модели
     * @var string
     */
    public $modelClass = 'app\models\User';
}
