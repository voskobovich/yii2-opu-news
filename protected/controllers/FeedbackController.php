<?php

namespace app\controllers;

use app\data\ActiveDataProvider;
use app\extensions\Frontend;
use app\forms\FeedbackForm;
use app\helpers\HttpError;
use app\models\Post;
use Yii;


/**
 * Class FeedbackController
 * @package app\controllers
 */
class FeedbackController extends Frontend
{
    /**
     * Вывод всех моделей
     * @return string
     */
    public function actionIndex()
    {
        $model = new FeedbackForm();

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $model->send();
            Yii::$app->session->setFlash('success');
        }

        return $this->render('index', ['model' => $model]);
    }
}