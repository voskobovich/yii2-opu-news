<?php

namespace app\controllers;

use app\data\ActiveDataProvider;
use app\extensions\Frontend;
use app\helpers\HttpError;
use app\models\Category;


/**
 * Class CategoryController
 * @package app\controllers
 */
class CategoryController extends Frontend
{
    /**
     * Вывод одной модели
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        /** @var \app\models\Category $model */
        $model = Category::findOne($id);

        if ($model == null) {
            HttpError::the404();
        }

        $query = $model->getPosts();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }
}