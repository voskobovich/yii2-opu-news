<?php

namespace app\controllers;

use app\data\ActiveDataProvider;
use app\extensions\Frontend;
use app\helpers\HttpError;
use app\models\Category;
use app\models\Post;


/**
 * Class SearchController
 * @package app\controllers
 */
class SearchController extends Frontend
{
    /**
     * Поиск поделей
     * @param $q
     * @return string
     */
    public function actionIndex($q)
    {
        $query = Post::find()
            ->visible()
            ->orderBy(['created_at' => SORT_DESC])
            ->andFilterWhere(['like', 'name', $q])
            ->orFilterWhere(['like', 'description', $q])
            ->orFilterWhere(['like', 'content', $q]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
}