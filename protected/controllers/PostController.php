<?php

namespace app\controllers;

use app\data\ActiveDataProvider;
use app\extensions\Frontend;
use app\helpers\HttpError;
use app\models\Post;


/**
 * Class PostController
 * @package app\controllers
 */
class PostController extends Frontend
{
    /**
     * Вывод всех моделей
     * @return string
     */
    public function actionIndex()
    {
        $query = Post::find()
            ->visible()
            ->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Вывод одной модели
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Post::find()
            ->andWhere(['id' => $id])
            ->visible()
            ->one();

        if ($model == null) {
            HttpError::the404();
        }

        return $this->render('view', ['model' => $model]);
    }
}