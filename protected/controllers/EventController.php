<?php

namespace app\controllers;

use app\extensions\Frontend;
use app\helpers\HttpError;
use app\models\Event;


/**
 * Class EventController
 * @package app\controllers
 */
class EventController extends Frontend
{
    /**
     * Вывод одной модели
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = Event::find()
            ->andWhere(['id' => $id])
            ->visible()
            ->one();

        if ($model == null) {
            HttpError::the404();
        }

        return $this->render('view', ['model' => $model]);
    }
}