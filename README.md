Yii2 Basic App
================================

Базовое приложение на Yii2

Для включения сортировки в gridview, нужно:

1. Добавить в контроллер (Заменить Model на имя модели):
```
    public function actions()
    {
        return [
            'sort' => [
                'class' => \app\actions\SortableGridAction::className(),
                'modelName' => Model::className(),
            ],
        ];
    }
```
2. Добавить в модель бихевер (Заменить position на название поля позиции):
```
    $behaviors['sort'] = [
        'class' => \app\behaviors\SortableGridBehavior::className(),
        'sortableAttribute' => 'position'
    ];
```
3. В выводе gridview добавить свойство:
```
is_sortable => true,
```
