<?php

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../protected/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../protected/config/web.php'),
    require(__DIR__ . '/../protected/config/web-local.php')
);

(new yii\web\Application($config))->run();