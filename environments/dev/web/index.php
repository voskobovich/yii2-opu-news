<?php

defined('YII_ENV_DEV') or define('YII_ENV_DEV', 'development');
defined('YII_DEBUG') or define('YII_DEBUG', YII_ENV_DEV == 'development');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../protected/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../protected/config/web.php'),
    require(__DIR__ . '/../protected/config/web-local.php')
);

(new yii\web\Application($config))->run();