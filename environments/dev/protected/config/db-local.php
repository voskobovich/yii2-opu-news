<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=opu_news_dev',
    'username' => 'opu_news_u',
    'password' => 'opu_news_p',
    'charset' => 'utf8',
    'tablePrefix' => '',
];