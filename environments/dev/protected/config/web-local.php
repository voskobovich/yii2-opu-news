<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
        'assetManager' => [
            'linkAssets' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];

if (!YII_ENV_TEST) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'] // adjust this to your needs
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
        'generators' => [
            'model' => [
                'class' => 'app\gii\generators\model\Generator',
                'templates' => [
                    'default' => '@app/gii/generators/model/default',
                    'base' => '@app/gii/generators/model/base',
                ]
            ]
        ],
    ];
}

return $config;